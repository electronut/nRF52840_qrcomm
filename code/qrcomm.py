###############################################################################
# qrcomm.py                                                                    
#                                                                               
# Author: electronut.in                                                         
#                                                                               
# Description:                                                                  
#                                                                               
# Commission a Thread Joiner from Pi OTBR by scanning QR code                                                   
# eg:                                                                           
#                                                                               
# python qrcomm.py 
# Found device: (2b749b2cc8427dd7, ENUT02)
# Joiner added.
# done.                                                               
#                                                                               
###############################################################################

import subprocess

# enable commissioning
def commission_enable():
  proc = subprocess.Popen(['wpanctl','commissioner', '-e'],
    stdout=subprocess.PIPE)  
  while True:
    line = proc.stdout.readline()
    if line != '':
      print(line)
    else:
      break

# perform commissioning
def commission_joiner(eui, cc):
  proc = subprocess.Popen(['wpanctl','commissioner', '--joiner-add',
    cc, eui],stdout=subprocess.PIPE)  
  while True:
    line = proc.stdout.readline()
    if line != '':
      print(line)
    else:
      break

def main():
  # start zbarcam process
  proc = subprocess.Popen(['zbarcam','--nodisplay', '--prescale=640x480'],
	  stdout=subprocess.PIPE)
  try:
    while True:
      line = proc.stdout.readline()
      if line != '':
        # data is of the form:
        # QR-Code:v=1&&eui=2b749b2cc8427dd7&&cc=ENUT02
        data = line.strip()
        #print(data)
        # parse
        i1 = data.find("eui=")
        eui = data[i1+1+3:i1+4+16]
        i2 = data.find("cc=")
        cc = data[i2+3:]
        print("Found device: (%s, %s)" % (eui, cc))
        commission_enable()
        commission_joiner(eui, cc)
        print("done.")
      break
  except Exception as ex:
    print(ex)
  # kill process
  subprocess.call(['sudo', 'kill', str(proc.pid), '-s', 'SIGINT'])
  exit(0)

# call main                                                                     
if __name__ == '__main__':
  main()