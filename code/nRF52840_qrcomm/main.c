/**
 * 
 * nRF52840_qrcomm
 * 
 * An example that demonstartes nRF52840 QR bsed Thread Commisioning
 * 
 * Mahesh Venkitachalam
 * electronut.in
 * 
 * Adapted from Nordic OpenThread MeshCoP example.
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "app_scheduler.h"
#include "app_timer.h"
#include "bsp_thread.h"
#include "nrf_assert.h"
#include "nrf_log_ctrl.h"
#include "nrf_log.h"
#include "nrf_log_default_backends.h"

#include "thread_utils.h"

#include <openthread/cli.h>
#include <openthread/joiner.h>
#include <openthread/openthread.h>
#include <openthread/platform/platform.h>

#define JOINER_DELAY             500
#define MAX_JOINER_RETRY_COUNT   3

#define APP_TIMER_PRESCALER      0                               // Value of the RTC1 PRESCALER register.
#define APP_TIMER_OP_QUEUE_SIZE  2                               // Size of timer operation queues.

#define EMPTY_VALUE              ""

#define SCHED_QUEUE_SIZE         32                              /**< Maximum number of events in the scheduler queue. */
#define SCHED_EVENT_DATA_SIZE    APP_TIMER_SCHED_EVENT_DATA_SIZE /**< Maximum app_scheduler event size. */

APP_TIMER_DEF(m_joiner_timer);

static uint8_t m_ndef_msg_buf[256];

typedef struct
{
    char psk_d[32];                    /**< Commissioning data structure. */
    uint32_t           joiner_retry_count;              /**< Retry counter for commissioning process. */
    bool               commissioning_in_progress;       /**< Indicates that the commissioning is ongoing. */
} application_t;

static application_t m_app =
{
    .psk_d = "ENUT02",
    .joiner_retry_count        = 0,
    .commissioning_in_progress = false,
};

/***************************************************************************************************
 * @ Callback functions
 **************************************************************************************************/

static void joiner_callback(otError error, void * p_context)
{
	uint32_t err_code;

    if (error == OT_ERROR_NONE)
    {
        err_code = bsp_thread_commissioning_indication_set(BSP_INDICATE_COMMISSIONING_SUCCESS);
        APP_ERROR_CHECK(err_code);

        error = otThreadSetEnabled(thread_ot_instance_get(), true);
        ASSERT(error == OT_ERROR_NONE);
    }
    else
    {
        if (m_app.joiner_retry_count < MAX_JOINER_RETRY_COUNT)
        {
            m_app.joiner_retry_count++;

            err_code = app_timer_start(m_joiner_timer, APP_TIMER_TICKS(JOINER_DELAY), thread_ot_instance_get());
            APP_ERROR_CHECK(err_code);
        }
        else
        {
            m_app.joiner_retry_count = 0;
            m_app.commissioning_in_progress = false;

            err_code = bsp_thread_commissioning_indication_set(
                BSP_INDICATE_COMMISSIONING_NOT_COMMISSIONED);
            APP_ERROR_CHECK(err_code);
        }
    }
}


static void bsp_event_handler(bsp_event_t event)
{
    switch (event)
    {
        case BSP_EVENT_KEY_2:
            {
                bsp_board_led_on(BSP_BOARD_LED_3);

                if (!m_app.commissioning_in_progress)
                {
                    m_app.commissioning_in_progress = true;

                    uint32_t err_code = bsp_thread_commissioning_indication_set(
                        BSP_INDICATE_COMMISSIONING_IN_PROGRESS);
                    APP_ERROR_CHECK(err_code);

                    err_code = app_timer_start(m_joiner_timer, APP_TIMER_TICKS(JOINER_DELAY), thread_ot_instance_get());
                    APP_ERROR_CHECK(err_code);
                }
            }
            break;

        case BSP_EVENT_KEY_3:
            otInstanceFactoryReset(thread_ot_instance_get());
            break;

        default:
            return;
    }
}

/***************************************************************************************************
 * @section State
 **************************************************************************************************/

static void thread_state_changed_callback(uint32_t flags, void * p_context)
{
    NRF_LOG_INFO("State changed! Flags: 0x%08x Current role: %d\r\n",
                 flags, otThreadGetDeviceRole(p_context));
}

/***************************************************************************************************
 * @section Timers
 **************************************************************************************************/

static void joiner_timer_handler(void * p_context)
{
    otError error = otJoinerStart(p_context,
                                  m_app.psk_d,
                                  EMPTY_VALUE,
                                  "Electronut Labs",
                                  EMPTY_VALUE,
                                  EMPTY_VALUE,
                                  EMPTY_VALUE,
                                  joiner_callback,
                                  p_context);
    ASSERT(error == OT_ERROR_NONE);
}

/***************************************************************************************************
 * @section Initialization

 **************************************************************************************************/

 /**@brief Function for initializing the Application Timer Module
 */
static void timer_init(void)
{
    uint32_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_joiner_timer, APP_TIMER_MODE_SINGLE_SHOT, joiner_timer_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the LEDs.
 */
static void leds_init(void)
{
    LEDS_CONFIGURE(LEDS_MASK);
    LEDS_OFF(LEDS_MASK);
}


/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing the Thread Board Support Package
 */
static void thread_bsp_init(otInstance * p_ot_instance)
{
    uint32_t err_code = bsp_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_thread_init(p_ot_instance);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Thread Stack
 */
static void thread_instance_init(void)
{
    thread_configuration_t thread_configuration =
    {
        .role              = RX_ON_WHEN_IDLE,
        .autocommissioning = false,
    };
    thread_init(&thread_configuration);
    thread_cli_init();
    thread_state_changed_callback_set(thread_state_changed_callback);
}


/**@brief Function for initializing scheduler module.
 */
static void scheduler_init(void)
{
    APP_SCHED_INIT(SCHED_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}

/***************************************************************************************************
 * @section Main
 **************************************************************************************************/

int main (int argc, char *argv[])
{
	uint32_t err_code;

    log_init();
    scheduler_init();
    timer_init();
    leds_init();

    thread_instance_init();
    thread_bsp_init(thread_ot_instance_get());

    if (!otDatasetIsCommissioned(thread_ot_instance_get()))
    {
        err_code = bsp_thread_commissioning_indication_set(
            BSP_INDICATE_COMMISSIONING_NOT_COMMISSIONED);
        APP_ERROR_CHECK(err_code);
    }
    else
    {
        err_code = bsp_thread_commissioning_indication_set(BSP_INDICATE_COMMISSIONING_SUCCESS);
        APP_ERROR_CHECK(err_code);
    }

    while (true)
    {
        thread_process();
        app_sched_execute();

        if (NRF_LOG_PROCESS() == false)
        {
            thread_sleep();
        }
    }
}

/**
 *@}
 **/
